<?php

require_once('./Controller/PollWidgetController.php');

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Poll 1</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='./assets/styles/reset.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='./assets/styles/poll-widget.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='./assets/styles/poll-page-2.css'>
</head>

<body>
    <div class="page-wrapper">
        <?php
            $pollId = '1';
            include './Widget/PollWidget.php';
        ?>
    </div>

    <!-- scripts -->
    <script>
        window.page = 'page-2';
    </script>
    <script src='./assets/scripts/poll-widget.js'></script>
</body>
</html>