<?php

class PollWidgetController {

    static $pollId = [];
    public function getPoll($pollId) : Array{

        $data =  $this->getPollData($pollId);

        if(in_array($pollId,self::$pollId) || empty($data)){
            return [];
        }

        if(empty($data['question'] || !is_array($data['answers'] || count($data['answers'])))){
            return [];
        }

        self::$pollId[]= $data['id'];
        return $data;
    }

    private function getPollData($pollId) : Array{
        $config = $this->getQuestions();

        $filteredData = array_values(array_filter($config,function($poll) use ($pollId){
            return $poll['id'] == $pollId;
        }));

        if(is_array($filteredData) && count($filteredData) > 0){
            return $filteredData[0];
        }

        return [];
    }

    private function getQuestions() : Array{
        if(file_exists('./Config/questions.json') === false){
            return [];
        }
        $config = file_get_contents('./Config/questions.json');
        if(empty($config)){
            return [];
        }
        
        $config = (json_decode($config,true));

        if(!empty($config['questions'])){
            return $config['questions'];
        }

        return [];
    }
}