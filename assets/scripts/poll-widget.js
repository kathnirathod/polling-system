(function () {
    setPollData();

    document.querySelectorAll('.poll-widget').forEach(element => {
        element.querySelectorAll('.poll-answer').forEach(answer => {
            answer.addEventListener("click", function (e){
                let pollId = e.target.closest(".poll-answer").getAttribute('data-poll-id');
                let answerId = e.target.closest(".poll-answer").getAttribute('data-answer-id');
                let updatedPoll = [];
                let pollDataFromLocalStorage = getLocalStorageData();
                if(pollDataFromLocalStorage && pollDataFromLocalStorage[pollId]){
                    updatedPoll = getUpdatedPollResult(pollId,answerId,pollDataFromLocalStorage[pollId])
                }
                else{
                    updatedPoll = getUpdatedPollResult(pollId,answerId)
                }
                saveToLocalStorage(pollId,updatedPoll);
                setPollData();
            })
        })
    });

})();
function saveToLocalStorage(pollId, updatedPoll){
    let widgetData = {};
    let currentPollData = getLocalStorageData();
    if(Object.keys(currentPollData).length > 0){
        currentPollData[pollId] = updatedPoll;
        widgetData = currentPollData;
        localStorage.setItem(window.page+"pollWidgetData", JSON.stringify(widgetData));
    }
    else{
        widgetData[pollId] = updatedPoll;
        localStorage.setItem(window.page+"pollWidgetData", JSON.stringify(widgetData));
    }
}

function getLocalStorageData(){
    let currentPollData = localStorage.getItem(window.page+"pollWidgetData");
    console.log(currentPollData);
    if(currentPollData && isJsonString(currentPollData)){
        return JSON.parse(currentPollData);
    }
    return {};
}

function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function getUpdatedPollResult(pollId, answerId, pollData = []){
    let data;
    console.log(pollData);

    if(Object.keys(pollData).length > 0){
        data = pollData;
    }
    else{
        data = window.pollData[pollId];
    }

    data['answers'][answerId]['count']++;
    data['totalCount']++;
    
    console.log(data);
    return data;
}

function setPollData(){
    let data = getLocalStorageData();
    data = Object.keys(data).map((key) => [key, data[key]]);
    data.forEach(poll => {
        let pollId = poll[0];
        let pollData = poll[1];

        for(let poll in pollData.answers){
            let perc = (pollData.answers[poll].count/pollData.totalCount)*(100);
            document.querySelector('#'+pollId+' .poll-answer[data-answer-id="'+poll+'"] .poll-answer-background').style.width = Math.round(perc)+'%';
            document.querySelector('#'+pollId+' .poll-answer[data-answer-id="'+poll+'"] .poll-answer-percentage').innerHTML = Math.round(perc)+'%';
            document.querySelector('#'+pollId+' .total-votes').innerHTML = pollData.totalCount;
            if(pollData.answers[poll].count == 1){
                document.querySelector('#'+pollId+' .poll-answer[data-answer-id="'+poll+'"] .poll-count').innerHTML = pollData.answers[poll].count+' Vote';
            }
            else{
                document.querySelector('#'+pollId+' .poll-answer[data-answer-id="'+poll+'"] .poll-count').innerHTML = pollData.answers[poll].count+' Votes';
            }
        }

    });
}