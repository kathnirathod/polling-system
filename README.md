# Polling System


## Setup

- [ ] Install Xampp with php
- [ ] Place this project inside xampp > htdocs
- [ ] Start Apache from xampp control
- [ ] The project will be running on localhost/{project-folder-name}
- [ ] Page 1 - localhost/{project-folder-name}/poll1
- [ ] Page 2 - localhost/{project-folder-name}/poll2

## Configure Questions

- Open /Config/questions.js
- To Edit Questions: Replcace "question" and "label" in "answers"
- To Add Questions: Copy Below json & append it in "questions" with replacing {id} with unique ID and replace question and answers

```
{
            "id" : {id},
            "question" : "{Question}",
            "answers" : [
                {
                    "label" : "{Answer1}",
                    "count" : 0
                },
                {
                    "label" : "{Answer2}",
                    "count" : 0
                },
                {
                    "label" : "{Answer3}",
                    "count" : 0
                }
            ],
            "totalCount" : 0
        },
```

