<?php 

$data = (new PollWidgetController())->getPoll($pollId);
if(empty($data)){
    return [];
}
?>
<div class="poll-widget" id="poll-<?php echo $data['id']; ?>">
    <div class='poll-container'>
        <div class='poll-wrapper'>
            <div class='poll-question'>
                <div><?php echo $data['question'] ?></div>
            </div>
            <div class='poll-answers'>
                <?php foreach ($data['answers'] as $key => $answer) { ?>
                    <div class='poll-answer' data-poll-id="poll-<?php echo $data['id']; ?>" data-answer-id="<?php echo $key; ?>">
                        <div class="poll-count">0 Votes</div>
                        <div class='poll-answer-background'></div>
                        <div class='poll-answer-label'><?php echo $answer['label']; ?></div>
                        <div class='poll-answer-percentage'>0%</div>
                    </div>
                <?php } ?>
            </div>
            <div class="poll-results">
                    Total Votes : <span class="total-votes">0</span>
            </div>
        </div>
    </div>
</div>

<script>
    if(!window.pollData){
       window.pollData = [];
    }
    window.pollData['poll-'+<?php echo $data['id']; ?>] = <?php  print_r(json_encode($data)); ?>;
</script>